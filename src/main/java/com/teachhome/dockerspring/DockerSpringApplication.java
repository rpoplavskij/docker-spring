package com.teachhome.dockerspring;

import com.teachhome.dockerspring.config.SwaggerConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Import({
        SwaggerConfig.class
})
@EnableSwagger2
@SpringBootApplication
public class DockerSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(DockerSpringApplication.class, args);
    }

}
